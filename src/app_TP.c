/*=======[app_TP]=============================================================
 * Author: Santiago Abbate <sabbate@unrn.edu.ar>
 * All rights reserved.
 * License: BSD-3-Clause <https://opensource.org/licenses/BSD-3-Clause>
 * Version: 1
 * Date: 2019/10/14
 *===========================================================================*/

/*=====[Inclusions of function dependencies]=================================*/

#include "app_TP.h"         // <= Su propia cabecera
#include "sapi.h"           // <= Biblioteca sAPI

/*=====[Definitions of external public global variables]=====================*/

// Estructura de configuración del generador de señales
extern dacGen_t dacGen;

// FUNCION PRINCIPAL, PUNTO DE ENTRADA AL PROGRAMA LUEGO DE ENCENDIDO O RESET.
int main( void )
{
   // ---------- CONFIGURACIONES ------------------------------

   // Inicializar y configurar la plataforma
   boardConfig();

   // Inicializar máquina de estados del menu por UART
   uartMenuFSMInit();

   // Inicializar los campos mínimos necesarios del dacGen
   dacGenSetTSampleUs(&dacGen,DEFAULT_TSAMPLE_US);    //Configuro tiempo de muestreo por default
   dacGenSetOffState(&dacGen,TRUE);                   //Dejo configurado como apagado

   // ---------- REPETIR POR SIEMPRE --------------------------
   while( TRUE ) {
      uartMenuFSMUpdate();
      // Uso un delay bloqueante porque las tareas de generación de señales del DAC
      // se manejan mediante interrupciones de timer.
      // La máquina de estados del menú a través de la uart también recibe por
      // interrupción los comandos. Como depende del usuario el ingreso de caracteres
      // me pareció razonable un delay de 200 ms para actualizar la máquina de estados
      delay(200);
   }

   return 0;
}
