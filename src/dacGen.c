/*=======[dacGen]=============================================================
 * Author: Santiago Abbate <sabbate@unrn.edu.ar>
 * All rights reserved.
 * License: BSD-3-Clause <https://opensource.org/licenses/BSD-3-Clause>
 * Version: 1
 * Date: 2019/10/14
 *===========================================================================*/

/*=====[Inclusion of own header]=============================================*/

#include "sapi.h"
#include "dacGen.h"

/*=====[Definition macros of private constants]==============================*/

#define SAMPLES 99

/*=====[Definitions of public global variables]==============================*/

dacGen_t dacGen;

/*=====[Definitions of private global variables]=============================*/

// Tabla de valores de la señal senoidal a generar
static const uint16_t lut[100] = {
    512, 544, 576, 608, 640, 671, 702, 731, 760, 788, 815, 840, 864, 887,
    908, 928, 946, 962, 977, 989, 1000, 1009, 1015, 1020, 1022, 1023, 1021, 1018,
    1012, 1005, 995, 983, 970, 954, 937, 919, 898, 876, 853, 828, 802, 774,
    746, 717, 686, 656, 624, 592, 560, 528, 495, 463, 431, 399, 367, 337,
    306, 277, 249, 221, 195, 170, 147, 125, 104, 86, 69, 53, 40, 28,
    18, 11, 5, 2, 0, 1, 3, 8, 14, 23, 34, 46, 61, 77,
    95, 115, 136, 159, 183, 208, 235, 263, 292, 321, 352, 383, 415, 447,
    479, 511};

/*=====[Prototypes (declarations) of private functions]======================*/

// Inicializa el Timer 0 con la frecuencia de muestreo asignada
void dacGenSampleTimerInit(uint32_t uS);

// Inicializa el Timer 1 para el modo Pulsado
// Parámetros:
//  tau:Cantidad de ciclos de la senoidal en un "Burst"
//  pr: Cantidad de "taus" que dura el período de repetición
void dacGenPulsedTimerInit(uint32_t tau, uint32_t pr);

void dacGenSampleTimerDisable();
void dacGenPulsedTimerDisable();

/*=====[Implementations of public functions]=================================*/

void dacGenSetModoPulsado(dacGen_t* dac,bool_t config)
{
    dac->modoPulsado = config;
}

void dacGenSetTSampleUs(dacGen_t* dac, uint32_t tSample)
{
    dac->tSample = tSample;
}

void dacGenSetOffState(dacGen_t* dac,bool_t config)
{
    dac->off = config;
}

void dacGenSetTau(dacGen_t* dac, uint32_t nPeriods)
{
    dac->tau = nPeriods;
}

void dacGenSetPr(dacGen_t* dac, uint32_t nTaus)
{
    dac->pr = nTaus;
}

// Función que inicializa el DAC y los TIMERs correspondientes tomando la configuración
// de la estructura dacGen
void dacGenInit(dacGen_t* gen)
{   
    // Duración en uS de cada "burst" = (Tiempo de muestreo * Cantidad de muestras) * Cantidad de ciclos
    uint32_t tauUs = (gen->tSample) * SAMPLES * (gen->tau);
    // Duración en uS de cada período de repetición = Tiempo del "burst" * Duración (en "cantidad de bursts")
    uint32_t prUs =  tauUs * (gen->pr);
    dacInit(DAC_ENABLE);
    // Inicio el timer que actualiza las muestras a sacar por el DAC
    dacGenSampleTimerInit(gen->tSample);
 
    // Inicializo el Timer del modo pulsado si corresponde
    if (gen->modoPulsado)
    {
        dacGenPulsedTimerInit(Timer_microsecondsToTicks(tauUs), Timer_microsecondsToTicks(prUs));
    }
    else
    {
        dacGenPulsedTimerDisable();
    }
}

// Configuración del timer que actualiza las muestras del DAC
// Recibe valor en microsegundos para interrupción del timer
void dacGenSampleTimerInit(uint32_t uS)
{
    Timer_Init(TIMER0,Timer_microsecondsToTicks(uS),onSampleTimerMatch);
}

void dacGenPulsedTimerInit(uint32_t tau_ticks, uint32_t pr_ticks)
{   
    Timer_Init(TIMER1,pr_ticks,onPulsedTimerMatch_EnableDac);
    Timer_EnableCompareMatch(TIMER1,1,tau_ticks,onPulsedTimerMatch_DisableDac);
}

void dacGenSampleTimerDisable()
{
    Chip_TIMER_Disable(LPC_TIMER0);
    NVIC_DisableIRQ(TIMER0_IRQn);
}

void dacGenPulsedTimerDisable()
{
    Chip_TIMER_Disable(LPC_TIMER1);
    NVIC_DisableIRQ(TIMER1_IRQn);
}

/*=====[Implementations of interrupt functions]==============================*/

// Función que se ejecuta cuando hay interrupción del TIMER0 (Timer de frecuencia de muestreo)
void onSampleTimerMatch()
{
    static uint8_t lutIndex = 0;

    // Actualizo la salida del DAC tomando muestras de la senoidal
    if (dacGen.off == FALSE)
    {
        dacWrite(DAC, lut[lutIndex]);
    }
    else    // Excepto el dacGen esté configurado como apagado
    {
        dacWrite(DAC,0);
        lutIndex = 0;
    }

    // Incremento el índice que toma las muestras de la senoidal
    if (lutIndex < (SAMPLES - 1))
    {
        lutIndex++;
    }
    else
    {
        lutIndex = 0;
    }
}

// ISR del Timer1, configuro dacGen para volver a generar las señales (Período de repetición cumplido)
void onPulsedTimerMatch_EnableDac()
{
    dacGen.off = FALSE;
}
// ISR del Timer1, configuro dacGen para deshabilitar la generación de señales (Tiempo del pulso cumplido)
void onPulsedTimerMatch_DisableDac()
{
    dacGen.off = TRUE;
}
/*=====[Implementations of private functions]================================*/
