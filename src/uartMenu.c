/*=====[uartMenu]=============================================================
 * Author: Santiago Abbate <sabbate@unrn.edu.ar>
 * All rights reserved.
 * License: BSD-3-Clause <https://opensource.org/licenses/BSD-3-Clause>
 * Version: 1
 * Date: 2019/10/14
 *===========================================================================*/


/*=====[Inclusion of own header]=============================================*/

#include "uartMenu.h"

/*=====[Inclusions of private function dependencies]=========================*/

#include "sapi.h"
#include "dacGen.h"

/*=====[Definition macros of private constants]==============================*/

// Comandos de modos, estos son los caracteres que se esperan a través de la UART
#define COMANDO_CONTINUO 'c'
#define COMANDO_PULSADO 'p'
#define COMANDO_RAPIDA 'r'
#define COMANDO_LENTA 'l'
#define COMANDO_INICIO 'i'
#define COMANDO_EMPEZAR 'e'
#define COMANDO_DETENER 'd'
#define COMANDO_PULSADO_A 'a'
#define COMANDO_PULSADO_B 'b'

// Constantes para los modos de operación pulsado (A ó B)
#define PULSADO_A_TAU   10
#define PULSADO_B_TAU   2
#define PULSADO_A_PR   10
#define PULSADO_B_PR   5

/*=====[Private function-like macros]========================================*/

#define enableCommandReceive(state);    menuPrint(state);uartInterrupt(UART_USB, TRUE);waitingCommand = TRUE;

/*=====[Definitions of external public global variables]=====================*/

// Estructura de configuración del generador de señales
extern dacGen_t dacGen;

/*=====[Definitions of private global variables]=============================*/

// Estado actual del menú del generador
static uartMenuState_t uartMenuState;

// Vector con punteros a los strings correspondientes a cada menú
static char *textIndex[NUM_STATES];
// Strings con el texto de cada menú
static char textStateInicio[] = "---\nMenu de inicio, seleccionar modo:\n\nContinuo: (c)\tPulsado: (p)";
static char textStateContinuo[] = "---\nModo continuo, configurar:\n\nFrecuencia Rapida (1 kHz): (r)\nFrecuencia lenta (10 Hz): (l)\nVolver a Inicio: (i)";
static char textStatePulsado[] = "---\nModo pulsado (Senoidal a 1kHz), seleccionar configuración:\n\nTau= 10 Periodos (10 ms), PRF= 10 (100 ms): (a)\nTau= 2 Periodos (2 ms), PRF= 5 Taus (10 ms): (b)\nVolver a Inicio: (i)";
static char textStateListo[] = "---\nGenerador Listo:\n\nEmpezar: (e)\tVolver a Inicio: (i)";
static char textStateGenerando[] = "---\nGenerando:\n\nDetener: (d)\tVolver a Inicio: (i)";
static char textComandoErroneo[] = "---\nCOMANDO ERRONEO: Ingresar nuevamente";
// Bandera para indicar si estoy esperando un comando
static bool_t   waitingCommand = FALSE;   
// Comando recibido
static char receivedCommand;

/*=====[Prototypes (declarations) of private functions]======================*/

// Esta función imprime el menú a través de la UART
// en función del estado actual
static void menuPrint(uartMenuState_t state);

// Esta función inicializa la UART y el vector que almacena las direcciones de los
// textos correspondientes al menú
static void menuTextInit();

/*=====[Implementations of public functions]=================================*/

void uartMenuFSMInit()
{
    menuTextInit();
    uartMenuState = STATE_INICIO;
}

void uartMenuFSMUpdate()
{
    // Entry leave variables, se ejecuta la acción si entryAction* es verdadera
    static bool_t entryActionInicio = TRUE;
    static bool_t entryActionContinuo = TRUE;
    static bool_t entryActionPulsado = TRUE;
    static bool_t entryActionListo = TRUE;
    static bool_t entryActionGenerando = TRUE;

    switch (uartMenuState)
    {
    /*======================== BEGIN:STATE INICIO ========================*/    
    case STATE_INICIO:
        /* Entry Actions */
        if (entryActionInicio)
        {
            entryActionInicio = FALSE;
            // Imprimo menú, hablilito la recepción del comando
            enableCommandReceive(STATE_INICIO);
        }

        if (!waitingCommand)  //Entro acá si recibí comando
        {   
            switch (receivedCommand)
            {
            case COMANDO_CONTINUO:
                uartMenuState = STATE_CONTINUO;
                break;
            case COMANDO_PULSADO:
                uartMenuState = STATE_PULSADO;
                break;
            default:
                waitingCommand = TRUE;       //Comando fué erroneo, vuelvo a esperar comando
                menuPrintDebug(textComandoErroneo);
                entryActionInicio = TRUE;
                break;
            }
        }       
        break;
    /*========================= END:STATE INICIO =========================*/
    
    /*======================= BEGIN:STATE CONTINUO =======================*/
    case STATE_CONTINUO:
        /* Entry Actions */
        if (entryActionContinuo)
        {
            entryActionContinuo = FALSE;
            // Imprimo menú, hablilito la recepción del comando
            enableCommandReceive(STATE_CONTINUO);
            dacGenSetModoPulsado(&dacGen,FALSE);
        }

        if (!waitingCommand)  //Entro acá si recibí comando
        {
            switch (receivedCommand)
            {
            case COMANDO_RAPIDA:
                dacGenSetTSampleUs(&dacGen,TSAMPLE_RAPIDO_US);
                uartMenuState = STATE_LISTO;
                break;
            case COMANDO_LENTA:
                dacGenSetTSampleUs(&dacGen,TSAMPLE_LENTO_US);
                uartMenuState = STATE_LISTO;
                break;
            case COMANDO_INICIO:
                uartMenuState = STATE_INICIO;
                entryActionInicio = TRUE;
                break;
            default:
                waitingCommand = TRUE;       //Comando fué erroneo, vuelvo a esperar comando
                menuPrintDebug(textComandoErroneo);
                break;
            }
            entryActionContinuo = TRUE;
        }
        break;
    /*======================== END:STATE CONTINUO ========================*/
    
    /*======================= BEGIN:STATE PULSADO ========================*/
    case STATE_PULSADO:
        /* Entry Actions */
        if (entryActionPulsado)
        {
            entryActionPulsado = FALSE;
            // Imprimo menú, hablilito la recepción del comando
            enableCommandReceive(STATE_PULSADO);
            dacGenSetModoPulsado(&dacGen,TRUE);
            // La frecuencia de la senoidal en el estado pulsado es única
            // por eso la seteo al entrar al estado
            dacGenSetTSampleUs(&dacGen,TSAMPLE_RAPIDO_US);      
        }

        if (!waitingCommand)  //Entro acá si recibí comando
        {
            switch (receivedCommand)
            {
            case COMANDO_PULSADO_A:
                // Configuro los parámetros del modo pulsado A
                dacGenSetTau(&dacGen,PULSADO_A_TAU);
                dacGenSetPr(&dacGen,PULSADO_A_PR);
                uartMenuState = STATE_LISTO;
                break;
            case COMANDO_PULSADO_B:
                // Configuro los parámetros del modo pulsado B
                dacGenSetTau(&dacGen,PULSADO_B_TAU);
                dacGenSetPr(&dacGen,PULSADO_B_PR);
                uartMenuState = STATE_LISTO;
                break;
            case COMANDO_INICIO:
                uartMenuState = STATE_INICIO;
                entryActionInicio = TRUE;
                break;
            default:
                waitingCommand = TRUE;       //Comando fué erroneo, vuelvo a esperar comando
                menuPrintDebug(textComandoErroneo);
                break;
            }
            entryActionPulsado = TRUE;
        }
        break;
    /*======================== END:STATE PULSADO =========================*/
    
    /*======================== BEGIN:STATE LISTO =========================*/
    case STATE_LISTO:
        /* Entry Actions */
        if (entryActionListo)
        {
            entryActionListo = FALSE;
            // Imprimo menú, hablilito la recepción del comando
            enableCommandReceive(STATE_LISTO);
        }

        if (!waitingCommand)  //Entro acá si recibí comando
        {
            switch (receivedCommand)
            {
            case COMANDO_EMPEZAR:
                uartMenuState = STATE_GENERANDO;
                break;
            case COMANDO_INICIO:
                uartMenuState = STATE_INICIO;
                entryActionInicio = TRUE;
                break;
            default:
                waitingCommand = TRUE;       //Comando fué erroneo, vuelvo a esperar comando
                menuPrintDebug(textComandoErroneo);
                break;
            }
            entryActionListo = TRUE;
        }
        break;
    /*======================== END:STATE LISTO ===========================*/
    
    /*======================== BEGIN:STATE GENERANDO =====================*/
    case STATE_GENERANDO:
        /* Entry Actions */
        if (entryActionGenerando)
        {
            entryActionGenerando = FALSE;
            // Imprimo menú, hablilito la recepción del comando
            enableCommandReceive(STATE_GENERANDO);
            dacGenSetOffState(&dacGen,FALSE);
            //Inicializo el dacGen al entrar al estado GENERANDO
            dacGenInit(&dacGen);
            gpioWrite(LED3,TRUE);
        }

        if (!waitingCommand)  //Entro acá si recibí comando
        {
            switch (receivedCommand)
            {
            case COMANDO_DETENER:
                uartMenuState = STATE_LISTO;
                break;
            case COMANDO_INICIO:
                uartMenuState = STATE_INICIO;
                entryActionInicio = TRUE;
                break;
            default:
                waitingCommand = TRUE;       //Comando fué erroneo, vuelvo a esperar comando
                menuPrintDebug(textComandoErroneo);
                break;
            }
            entryActionGenerando = TRUE;

            /* Leave Actions */
            if (uartMenuState != STATE_GENERANDO)
            {
                dacWrite(DAC,0);
                dacInit(DAC_DISABLE);
                dacGenSetOffState(&dacGen,TRUE);
                gpioWrite(LED3,FALSE);
            }
            
        }
        break;
    /*======================== END:STATE GENERANDO =======================*/
    default:
        uartMenuState = STATE_INICIO;
        entryActionInicio = TRUE;
        break;
    }
}

/*=====[Implementations of interrupt functions]==============================*/

// Función que se ejecuta cuando hay interrupción por recepción de la UART. 
void onRx( void *noUsado )
{   
    // Quiero leer un único caracter cuando estoy esperando un comando  
    if (waitingCommand){
        receivedCommand = uartRxRead( UART_USB );
    }
    waitingCommand = FALSE;
    
    // Medida simple para evitar usuarios que aprieten muchas teclas a la vez
    // Vacío el buffer de RX de la UART, ya que sólo me intersa el primer comando enviado
    char clearBuffer[20];
    uint8_t i = 0;
    //Vacío la RX FIFO
    while (uartRxReady(UART_USB))
    {
        clearBuffer[i] = uartRxRead(UART_USB);
        i++;
    }      
}

/*=====[Implementations of private functions]================================*/

// Esta función imprime el menú a través de la UART
// en función del estado actual
void menuPrint(uartMenuState_t state)
{
    menuPrintDebug(textIndex[state]);
}

// Esta función inicializa la UART y el vector que almacena las direcciones de los
// textos correspondientes al menú
void menuTextInit()
{
    // Inicializo la UART
    uartConfig(UART_USB, 115200);
    // Seteo un callback al evento de recepcion y habilito su interrupcion
    uartCallbackSet(UART_USB, UART_RECEIVE, onRx, NULL);

    textIndex[STATE_INICIO] = textStateInicio;
    textIndex[STATE_CONTINUO] = textStateContinuo;
    textIndex[STATE_PULSADO] = textStatePulsado;
    textIndex[STATE_LISTO] = textStateListo;
    textIndex[STATE_GENERANDO] = textStateGenerando;
}