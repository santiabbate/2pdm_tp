# 2PdM_TP

Trabajo final de la materia Programación de Microporcesadores.

## "Control del DAC de la CIAA a través de la UART"

### Resumen
Implementar un sistema sencillo que reciba comandos a través de la interfaz UART y configure el DAC para generar señales senoidales en distintos modos de operación.

Los modos de operación son un subset de una operación análoga a la del sistema del trabajo final de la CESE.

1.  Idle: DAC apagado 
2.  Onda continua
    * Frecuencia: Dos frecuencias, rápida (1 kHz) y lenta (10 Hz).
3.  Pulsado (A frecuencia de la senoidal fija = 1 kHz)
    Genera un "burst" de ciclos de la senoidal a una frecuencia dada por el período de repetición.
    * Modo A:
        * TBurst = 10 períodos de la senoidal (10 ms)
        * Período de repetición = 10 Bursts (100 ms)
    * Modo B:
        * TBurst = 2 períodos de la senoidal (2 ms)
        * Período de repetición = 5 Bursts (10 ms)


### Comandos
1. Ir a 'Inicio'
4. Modo "Continuo"
5. Modo "Pulsado"
2. Start
3. Stop
6. Selección de modo

### Máquina de estados propuesta                                              

[MEF- Menú de configuración a través de la UART](https://docs.google.com/drawings/d/16L44la8M68BRQEGBMAKvwIQaaDpBvi5gBEyVLzpi2fk/edit?usp=sharing)

![MEF](img/MEF.jpg)

### Modularización

* Archivo app_TP
* Archivo uartMenu: Máquina de estados del menú de configuración
* Archivo dacGen: Funciones de configuración del dac
    * La generación de las ondas se hace a través de dos timers:
    * TIMER0: Se interrumpe a intervalos de la frecuencia de muestreo para actualizar el valor de salida del DAC de la CIAA.
    * TIMER1: Se utiliza en el modo pulsado únicamente y se interrumpe en dos valores de comparación. El primero para dar la señal de apagado al DAC luego del tiempo "Tburst" que da la cantidad de períodos a transmitir. El segundo para reactivar el dac una vez se cumple el período de repetición.  

### Interfaces
* UART DEBUG de la EDU-CIAA
    * 115200 baudios
    * 8 bits
    * Sin bit de paridad
    * 1 bit de stop
    * Configurada \CR\LF

* DAC de la EDU-CIAA para salida de las señales
* LED3 de la EDU-CIAA se enciende en modo GENERANDO (Dac generando las señales)

### Mediciones

### Modo Continuo, Frecuencia "Rápida" - 1kHz

![Continuo Rápida](img/continuo_rapida.jpg)

### Modo Continuo, Frecuencia "Lenta" - 10Hz

![Continuo Lenta](img/continuo_lenta.jpg)

### Modo Pulsado, Modo A, Tburst 10 ms

![Pulsado A](img/pulsado_a_tburst.jpg)

### Modo Pulsado, Modo A, Período de repetición 100 ms

![Pulsado A](img/pulsado_a_periodo.jpg)

### Modo Pulsado, Modo B, Tburst 2 ms

![Pulsado A](img/pulsado_b_tburst.jpg)

### Modo Pulsado, Modo B, Período de repetición 10 ms

![Pulsado A](img/pulsado_b_periodo.jpg)

### Ejemplo de captura en CuteCom

![CuteCom](img/Captura_CuteCom.png)

### Video de ejemplo de uso
(https://gitlab.com/santiabbate/2pdm_tp/blob/master/img/Videoejemplo.mp4?expanded=true&viewer=rich)