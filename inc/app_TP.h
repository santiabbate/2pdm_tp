/*=======[app_TP]=============================================================
 * Author: Santiago Abbate <sabbate@unrn.edu.ar>
 * All rights reserved.
 * License: BSD-3-Clause <https://opensource.org/licenses/BSD-3-Clause>
 * Version: 1
 * Date: 2019/10/14
 *===========================================================================*/

#ifndef _APP_TP_H_
#define _APP_TP_H_

/*==================[inclusiones]============================================*/
#include "dacGen.h"
#include "uartMenu.h"

/*==================[end of file]============================================*/
#endif /* _APP_TP_H_ */
