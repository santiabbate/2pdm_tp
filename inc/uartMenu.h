/*=====[uartMenu]=============================================================
 * Author: Santiago Abbate <sabbate@unrn.edu.ar>
 * All rights reserved.
 * License: BSD-3-Clause <https://opensource.org/licenses/BSD-3-Clause>
 * Version: 1
 * Date: 2019/10/14
 *===========================================================================*/

/*=====[Avoid multiple inclusion - begin]====================================*/

#ifndef _UART_CONSOLE_H_
#define _UART_CONSOLE_H_

/*=====[Inclusions of public function dependencies]==========================*/

/*=====[C++ - begin]=========================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*=====[Definition macros of public constants]===============================*/
#define NUM_STATES  5
/*=====[Public function-like macros]=========================================*/

// Redefinición de "printf()"
#define menuPrintDebug(string)       printf("%s\r\n",string);

/*=====[Definitions of public data types]====================================*/
typedef enum
{
   STATE_INICIO,
   STATE_CONTINUO,
   STATE_PULSADO,
   STATE_LISTO,
   STATE_GENERANDO
} uartMenuState_t;

/*=====[Prototypes (declarations) of public functions]=======================*/
void uartMenuFSMInit();

void uartMenuFSMUpdate();

/*=====[Prototypes (declarations) of public interrupt functions]=============*/
void onRx( void *noUsado );
/*=====[Avoid multiple inclusion - end]======================================*/

#endif /* _UART_COSOLE_H_ */
