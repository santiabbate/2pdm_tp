/*=======[dacGen]=============================================================
 * Author: Santiago Abbate <sabbate@unrn.edu.ar>
 * All rights reserved.
 * License: BSD-3-Clause <https://opensource.org/licenses/BSD-3-Clause>
 * Version: 1
 * Date: 2019/10/14
 *===========================================================================*/

/*=====[Avoid multiple inclusion - begin]====================================*/

#ifndef _DAC_GEN_H_
#define _DAC_GEN_H_

/*=====[Inclusions of public function dependencies]==========================*/
#include "sapi.h"
/*=====[C++ - begin]=========================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*=====[Definition macros of public constants]===============================*/
#define GEN_MODO_A TRUE
#define GEN_MODO_B FALSE

#define TSAMPLE_RAPIDO_US 10
#define TSAMPLE_LENTO_US 1000

#define DEFAULT_TSAMPLE_US  10

/*=====[Public function-like macros]=========================================*/

/*=====[Definitions of public data types]====================================*/

// Estructura con parámetros de configuración del dacGen
typedef struct dacGen
{
    uint32_t tSample;   // Tiempo entre muestras del DAC  
    uint32_t tau;       // Duración del pulso (Tburst), en cantidad de períodos
    uint32_t pr;        // Duración del período de repetición, en cantidad pulsos (taus)
    bool_t  modoPulsado;
    bool_t off;         // Estado apagado del dacGen
}dacGen_t;

/*=====[Prototypes (declarations) of public functions]=======================*/

// Métodos para acceso a la estructura de configuración de dacGen
void dacGenSetOffState(dacGen_t* dac,bool_t config);

void dacGenSetTSampleUs(dacGen_t* dac, uint32_t tSample);

void dacGenSetModoPulsado(dacGen_t* dac,bool_t config);

void dacGenSetTau(dacGen_t* dac, uint32_t nPeriods);

void dacGenSetPr(dacGen_t* dac, uint32_t nTaus);

// Función que inicializa el DAC y los TIMERs correspondientes tomando la configuración
// de la estructura dacGen
void dacGenInit(dacGen_t* gen);

/*=====[Prototypes (declarations) of public interrupt functions]=============*/

void onSampleTimerMatch();
void onPulsedTimerMatch_EnableDac();
void onPulsedTimerMatch_DisableDac();

/*=====[C++ - end]===========================================================*/

#ifdef __cplusplus
}
#endif

/*=====[Avoid multiple inclusion - end]======================================*/

#endif /* _DAC_GEN_H_ */
